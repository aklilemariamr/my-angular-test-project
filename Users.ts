export class Users
{
    id:string;
    name:string;
    phone:string;
    email:string;
    municipality:string;
    Nationalid:string;
    address:string;
    serialnumber:string;
    senton:string;
    status:string;
    constructor(id,name, phone, email, municipality, Nationalid,address ,serialnumber,senton,status)
    {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.municipality = municipality;
        this.Nationalid = Nationalid;
        this.address = address;
        this.serialnumber = serialnumber;
        this.senton = senton;
        this.status = status;
    }
}