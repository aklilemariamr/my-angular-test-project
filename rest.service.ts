import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Users } from './Users';
import { Observable } from 'rxjs';
import { USERS } from './UUSERS';

@Injectable({

  providedIn: 'root'
})
export class RestService {

  private REST_API_SERVER = "http://localhost:3000/Users";

  constructor(private httpClient : HttpClient) { }

  url : string = "http://localhost:3000/Users";
  getUsers()
  {
    return this.httpClient.get<Users[]>(this.url);
  }
  public sendGetRequest():Observable<USERS[]>{
    return this.httpClient.get<USERS[]>(this.REST_API_SERVER);
  }

}
