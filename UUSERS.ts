export interface USERS{
  
    id: String;
    name: String;
    email: String;
    phone: String;
    Nationalid: String;
    municipality: String;
    address: String;
    serialnumber: String;
    senton: String;
    status: String;
}