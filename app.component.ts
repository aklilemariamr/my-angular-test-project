import { Component, OnInit } from '@angular/core';
import{RestService} from './rest.service';
import { Users } from './users';
import {jsPDF} from 'jspdf';
import 'jspdf-autotable';
import Map from 'ol/Map';
import View from 'ol/View';
import VectorLayer from 'ol/layer/Vector';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import OSM from 'ol/source/OSM';
import * as olProj from 'ol/proj';
import TileLayer from 'ol/layer/Tile';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  latitude: number = 18.5204;
  longitude: number = 73.8567;

  map: any;

  title = 'JSONServer';
  columns = ["User Id","Name","phone", "Email", "municipality", "Nationalid", "address", "serialnumber", "Senton", "Status"];
  index = ["id", "Name", "phone", "email", "municipality", "Nationalid", "address", "serialnumber", "Status"];
  //aklile :USERS[];
   //public students : any=[];
   public students=[];
   //public user:Users=this.students;
  constructor(private restService: RestService) { }
  users: Users []=[];
  ngOnInit() {
   
    this.restService.sendGetRequest().subscribe(data =>{console.log(data);this.students  = data});
    this.restService.getUsers().subscribe((response)=>{this.users = response;},
    (error) => 
   {
     console.log("error occurred :" +error);
   }
  )
  this.map = new Map({
    target: 'hotel_map',
    layers: [
      new TileLayer({
        source: new OSM()
      })
    ],
    view: new View({
      center: olProj.fromLonLat([7.0785, 51.4614]),
      zoom: 5
    })
  });
  }
    // {
   // this.restService.sendGetRequest().subscribe(data =>{
     // console.log(data);
      //this.students  = data;
   // })
      
     
      //this.arrBirds = data as string [];

    //}
  //}
  //constructor(private rs : RestService) { }
 // columns = ["User Id","Name","phone", "Email", "municipality", "Nationalid", "address", "serialnumber", "Senton", "Status"];
  //index = ["id", "Name", "phone", "email", "municipality", "Nationalid", "address", "serialnumber", "Status"];
  //users : Users[] = [];
  
  // tempArray: Users[]= [];
  //ngOnInit(): void {
    //var temp: Users[] = [];
    //this.rs.getUsers().subscribe
    //(
      //(response)=>
     // {
        
       // this.users = response;
        //temp=response;
        //this.students =response as USERS[];
     // },
      //(error) => 
     // {
      // console.log("error occurred :" +error);
    //  }
   // )

 // }

  //head= ["UserId","Name","phone", "Email", "municipality", "Nationalid", "address", "serialnumber", "Senton", "Status"];
  convert() {
    var doc = new jsPDF();

    //doc.setFontSize(18);
   // doc.text('My Team Detail', 11, 8);
    //doc.setFontSize(11);
   // doc.setTextColor(100);
    //this.students['id']

    //(doc as any).autoTable({
    //head: this. head,
     // body: this.students,
     //theme: 'plain',
      //didDrawCell: students => {
      //console.log(students.column.index)
   // }
   // })
   var col = ["UserId","Name","phone", "Email", "municipality", "Nationalid", "address", "serialnumber", "Senton", "Status"];
   var rows = [];
   var itemNew ;
  this.students .forEach(element => {
     itemNew = [    
      { id: this.students["id"], name : this.students["name"],email : this.students["email"] }
    ]
    itemNew.forEach(element => {      
      var temp = [element.id,element.name,element.email];
      rows.push(temp);
  
  });
  }); 
  //itemNew.forEach(element => {      
   // var temp = [element.id,element.name];
   // rows.push(temp);

//}); 
(doc as any).autoTable(col, rows);
doc.save('Test.pdf');
}
    // below line for Open PDF document in new tab
   // doc.output('dataurlnewwindow');

    // below line for Download PDF document  
  //  doc.save('myteamdetail.pdf');
  //}


}
